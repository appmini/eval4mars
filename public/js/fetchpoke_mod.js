const pokeP = document.getElementById('pokeInfo');
const pokeAbilityBtn = document.getElementById('ability');

// Définition de la fonction fetchPokemon
const fetchPokemon = async () => {
	const pokedexNum = Math.floor(Math.random() * 897);
	let foundPokemon = '';
	let jsonPokemon = '';
	const pokeInfo = {};

	try {
		foundPokemon = await fetch(
			`https://pokeapi.co/api/v2/pokemon/${pokedexNum}`,
			{
				method: 'GET',
				headers: {'Content-Type': 'application/json'},
			}
		);

		if (foundPokemon) {
			jsonPokemon = await foundPokemon.json();
			pokeInfo.name = `${jsonPokemon.species.name
				.charAt(0)
				.toUpperCase()}${jsonPokemon.species.name
				.slice(1)
				.toLowerCase()}`;
		} else {
			jsonPokemon = 'No Pokémon found...';
		}

		if (pokeP.innerText !== '') {
			pokeP.innerText = '';
		}

		pokeP.innerText = `Your Pokémon is ${pokeInfo.name}.`;
		pokeAbilityBtn.removeAttribute('disabled');
	} catch (error) {
		console.error(error.message);
	}
};

// Export de la fonction fetchPokemon
export default fetchPokemon;
