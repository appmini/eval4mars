// Définition de la fonction fetchPokemonAbilities
const fetchPokemonAbilities = async () => {
	const pokedexNum = Math.floor(Math.random() * 266);
	let foundAbilities = '';
	const pokeAbility = document.getElementById('pokeAbility');
	let jsonAbilities = {};
	let abilityToDisplay = '';

	try {
		foundAbilities = await fetch(
			`https://pokeapi.co/api/v2/ability/${pokedexNum}`,
			{
				method: 'GET',
				headers: {'Content-Type': 'application/json'},
			}
		);

		if (foundAbilities) {
			jsonAbilities = await foundAbilities.json();
			abilityToDisplay = jsonAbilities.name
				? `${jsonAbilities.name
						.charAt(0)
						.toUpperCase()}${jsonAbilities.name
						.slice(1)
						.toLowerCase()}`
				: 'Tackle';
		} else {
			jsonAbilities = 'No ability found...';
		}

		if (pokeAbility.innerText !== '') {
			pokeAbility.innerText = '';
		}

		pokeAbility.innerText = `It now knows the move ${abilityToDisplay}!`;
	} catch (error) {
		console.error(error.message);
	}
};

// Export de la fonction fetchPokemonAbilities
export default fetchPokemonAbilities;
