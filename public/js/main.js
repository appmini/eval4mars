import fetchPokemon from './fetchpoke_mod.js';
import fetchPokemonAbilities from './fetchAbilty_mod.js';

window.addEventListener('DOMContentLoaded', () => {
	/**
	 * Constantes permettant d'agir dans le DOM
	 */

	const pokeP = document.getElementById('pokeInfo');
	const pokeDiv = document.getElementById('pokemon-info');
	const pokeAbilityBtn = document.getElementById('ability');

	/**
	 * Fonctions permettant d'agir dans le DOM
	 */
	const invoquePokemon = () => {
		const pokeBtn = document.getElementById('pokemon');
		pokeBtn.addEventListener('click', fetchPokemon);
		pokeDiv.appendChild(pokeP);
	};

	const pokemonAbility = () => {
		pokeAbilityBtn.addEventListener('click', fetchPokemonAbilities);
		pokeDiv.appendChild(pokeAbility);
	};

	/**
	 * startAll est une fonction auto-invoquée (IIFE) qui permet de lancer les2  fonctions ci-dessus automatiquement
	 */

	(function startAll() {
		invoquePokemon();
		pokemonAbility();
	})();
});
