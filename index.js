/**
 * Importation des modules nécessaires à l'application
 */
import express from 'express';
import path from 'path';
import {fileURLToPath} from 'url';
import cors from 'cors';

/**
 * Définition des variables de chemin et de port
 */
const filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(filename);
const port = 8070;
const host = '127.0.0.1';

/**
 * Creation du serveur Express.

 */
const app = express();
/* Middleware pour renforcer la sécurité de l'application en définissant divers en-têtes HTTP.
 * @param {Function} helmet - The Helmet middleware function.
 */

/**
 * Configuration de l'application Express.
 * ici les chemins pour les fichiers statiques
 */

/**
 * Middleware pour parser les requetes entrantes et peupler l'object  req.body .
 * @param {Object} express.urlencoded - Pour PARSER  urlencoded data.
 * @param {boolean} extended - Indique d'utiliser la librairie  "querystring parsing (true)".
 */
app.use(express.urlencoded({extended: true}));
/**
 * Middleware pour permettre le Cross-Origin Resource Sharing.
 */
app.use(cors());
/**
 * Middleware pour servir les fichiers statics à partir du dossier 'public'.
 * @param {string} path - Chemin d'acces sous forme de string au dossier public.
 */
app.use(express.static(path.join(dirname, 'public')));

/**
 * Middleware to serve the favicon icon.
 * @param {string} '/favicon.ico' - Route pour atteindre le fichier de favicon.
 * @param {string} path.join(dirname, 'public', 'images', 'favicon.png') - Chemin d'acces vers les fichier favicon.
 */
app.use(
	'/favicon.ico',
	express.static(path.join(dirname, 'public', 'images', 'favicon.png'))
);

/**
 * Route pour le verbe GET de la requete avec la route racine du site.
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 */

app.get('/', (req, res) => {
	res.sendFile('index.html', {root: path.join(dirname)}, (err) => {
		if (err) throw new Error(err);
	});
});

/**
 * Route  pour la requete POST du formulaire avec la route /comment.
 * @param {Object} req - Object request.
 * @param {Object} res - Objet response.
 */

app.post('/comment', (req, res) => {
	const comment = req.body.message;
	// Nettoyage des données d'entrée
	const secure = comment
		.replace(/[<>]/g, '') //pour les chev
		.replace(/&/g, '&amp;') //pour l'affichage HTML
		.replace(/</g, '&lt;') //pour l'affichage HTML
		.replace(/>/g, '&gt;') //pour l'affichage HTML
		.replace(/"/g, '&quot;') //guillemets pour HTML
		.replace(/'/g, '&#x27;') // pour ne pas que le texte soit interprete en HTML comme une balise ou un symbol special
		.replace(/`/g, '&#x60;'); //pour échapper les backticks dans du contenu HTML,pour afficher  correctement
	console.log('🚀 ~ app.post ~ secure:', secure);

	// Enregistrer le commentaire désinfecté dans la base de données ou effectuer d'autres actions nécessaires
	return res.send(secure);
});

/**
 * Lancement du serveur
 */
app.listen(port, host, () => {
	console.info(`Server started @ http://${host}:${port}.`);
});
