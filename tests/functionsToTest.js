/**
 * Return un objet avec des indices pour les 'keys' et des arguments pour les valeurs associées.
 * @param {...any} args - Les arguments à intégrer à l'objet.
 * @returns {Object|string} - Un objet (voir ligne 2) OU , ou une chaine de caractere indiquant qu'aucun argument n' a été fourni.
 */
const returnAnObject = (...args) => {
	let response = {};
	if (args.length) {
		let index = 0;
		args.forEach((arg) => {
			response[index] = arg;
			index++;
		});
	} else {
		response = 'No argument was given to the function.';
	}
	return response;
};

/**
 * Multiplie chaque nombre du tableau initial (arrayOfNumbers) par 2 pour creer un NOUVEL OBJET TABLEAU (copie profonde) objet tableau  taleau avec les resultats.
 * @param {number[]} arrayOfNumbers - Un tableau de nombres multipliés par 2 à partir du tableau initial.
 * @returns {(number[]|string)} - Retourne soit un tableau de nombre soit une chaîne de caractères comme quoi le tableau initial est vide.
 */
const multiplyAllByTwo = (arrayOfNumbers) => {
	let response;
	if (
		arrayOfNumbers.constructor.prototype ===
		new Array().constructor.prototype
	) {
		response = arrayOfNumbers.map((val) => val * 2);
		console.log('arrayTimesTwo: ', response);
	} else {
		response = 'The argument is not an Array of numbers';
	}
	return response;
};

module.exports = {returnAnObject, multiplyAllByTwo};
