export default {
	testEnvironment: 'node',
	collectCoverage: true,
	coverageDirectory: 'couverture',
	coverageProvider: 'v8',
};
