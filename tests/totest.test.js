// returnAnObject.test.js
// import { returnAnObject } from './functionsToTest';
const {returnAnObject, multiplyAllByTwo} = require('./functionsToTest.js');

// const {} = require('../');
// const multiplyAllByTwo = require('../multiplyAllByTwo');

describe('fonction returnAnObject', () => {
	test('doit retourner un objet avec des pairs clés - valeurs', () => {
		const result = returnAnObject('key1', 'value1', 'key2', 'value2');
		expect(result).toEqual({
			0: 'key1',
			1: 'value1',
			2: 'key2',
			3: 'value2',
		});
	});

	test('retourne un message de type string si aucun argument fourni', () => {
		const result = returnAnObject();
		expect(result).toBe('No argument was given to the function.');
	});
});

describe('multiplyAllByTwo', () => {
	it('doit retourner un tableau avec les valaurs en double ', () => {
		const inputArray = [1, 2, 3];
		const expectedOutput = [2, 4, 6];
		expect(multiplyAllByTwo(inputArray)).toEqual(expectedOutput);
	});

	it("Doit renvoyer un message d'erreur si l'argument n'est pas un tableau de nombres", () => {
		const inputArray = 'not an array';
		const expectedOutput = 'The argument is not an Array of numbers';
		expect(multiplyAllByTwo(inputArray)).toEqual(expectedOutput);
	});
});
